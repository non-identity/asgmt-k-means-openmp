SHELL = /bin/bash
CXXFLAGS = -std=c++11
CXXFLAGS_OMP = -std=c++11 -fopenmp

all: run.out run_omp.out

run.out: main.cpp k_means_lloyd.o k_means_lloyd.hpp timer.o timer.hpp
	$(CXX) $(CXXFLAGS) -o $@ $^

run_omp.out: main.cpp k_means_lloyd_omp.o k_means_lloyd.hpp timer.o timer.hpp
	$(CXX) $(CXXFLAGS_OMP) -o $@ $^

k_means_lloyd.o: k_means_lloyd.cpp k_means_lloyd.hpp
	$(CXX) $(CXXFLAGS) -c $^

k_means_lloyd_omp.o: k_means_lloyd.cpp k_means_lloyd.hpp
	$(CXX) $(CXXFLAGS_OMP) -c $< -o $@

timer.o: timer.cpp timer.hpp
	$(CXX) $(CXXFLAGS) -c $^

run: run.out run_omp.out
	mkdir -p output
	for n in 1 2 3 4 5 ; do \
		./run.out -i data/data_500k.csv -o output/output.txt \
			-r output/result.txt -t output/time_$${n}.txt ; \
	done
	for s in STATIC DYNAMIC ; do \
		export OMP_SCHEDULE="$${s}"; \
		for t in 2 4 8 16 ; do \
			export OMP_NUM_THREADS=$${t} ; \
			for n in 1 2 3 4 5 ; do \
				./run_omp.out -i data/data_500k.csv -o output/output_omp.txt \
					-r output/result_omp.txt \
					-t output/time_omp_$${s}_$${t}_$${n}.txt ; \
			done \
		done \
	done

.PHONY: analyse, clean

analyse:
	python3 python3/analyse.py

clean:
	rm -f *.gch
	rm -f *.out
	rm -f *.o
	rm -f output/*
