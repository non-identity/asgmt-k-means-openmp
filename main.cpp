#include <unistd.h>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include "k_means_lloyd.hpp"
#include "timer.hpp"

void ReadPointsFromFile(std::vector<Point> &point_list,
                        std::ifstream &data_file) {
    std::string line;
    while (std::getline(data_file, line)) {
        std::stringstream s_stream(line);
        float x, y;
        std::string val;
        std::getline(s_stream, val, ',');
        x = std::atof(val.data());
        std::getline(s_stream, val, ',');
        y = std::atof(val.data());
        point_list.push_back(std::make_pair(x, y));
    }
}

int main(int argc, char **argv) {
    std::string input_file_name, output_file_name, result_file_name,
        time_file_name;
    int cmd_flag;
    while ((cmd_flag = getopt(argc, argv, "i:o:r:t:")) != -1) {
        switch (cmd_flag) {
            case 'i':
                input_file_name.assign(optarg);
                break;
            case 'o':
                output_file_name.assign(optarg);
                break;
            case 'r':
                result_file_name.assign(optarg);
                break;
            case 't':
                time_file_name.assign(optarg);
                break;
            default:
                std::cerr << "Invalid command line argument" << std::endl;
                exit(1);
        }
    }
    std::ifstream input_file(input_file_name);
    std::ofstream output_file(output_file_name), result_file(result_file_name),
        time_file(time_file_name);
    if (!input_file || !output_file || !result_file || !time_file) {
        std::cerr << "Error opening one or more files" << std::endl;
        exit(1);
    }
    std::vector<Point> point_list;
    ReadPointsFromFile(point_list, input_file);
    KMeansLloyd k_means_algorithm(20, point_list, 50000);
    Timer timer;
    int num_iter = k_means_algorithm.Compute();
    time_file << timer.GetElapsedTime() << std::endl;
    time_file << num_iter << std::endl;
    k_means_algorithm.PrintPointsWithMeanId(output_file);
    k_means_algorithm.PrintMeansWithPointCount(result_file);
    return 0;
}
