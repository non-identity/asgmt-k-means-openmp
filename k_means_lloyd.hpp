#include <fstream>
#include <vector>

typedef std::pair<float, float> Point;

class KMeansLloyd {
   public:
    KMeansLloyd(int num_clusters, std::vector<Point> &point_list, int max_iter);
    int Compute();
    void PrintMeansWithPointCount(std::ofstream &file);
    void PrintPointsWithMeanId(std::ofstream &file);

   private:
    void InitializeMeans();
    int NearestMean(Point &point);
    float L2DistanceSquared(Point &point_1, Point &point_2);
    int max_iterations;
    int points_count;
    int clusters_count;
    std::vector<Point> points;
    std::vector<int> mean_id_of_point;
    std::vector<Point> means;
    std::vector<int> num_points_in_cluster;
};
